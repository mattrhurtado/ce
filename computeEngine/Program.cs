﻿using System;
using System.Collections;
using System.Threading;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace computeEngine
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.ReadLine();
        }
    }

    public static class ComputeEngine
    {
        private static IDictionary<Guid, Results> resultList;

        // Determines if a specified job has been completed.
        // Throws ObjectNotFoundException for an unknown job.
        public static bool IsJobFinished(Guid jobID)
        {
            if (!resultList.ContainsKey(jobID))
            {
                throw new System.Data.ObjectNotFoundException();
            }

            Results ResultSet;
            return resultList.TryGetValue(jobID, out ResultSet);
        }

        // Retuns the completed job results.
        // Throws ObjectNotFOundException for an unknown job.
        // Throws InvalidOperationException if the job is not finished.
        public static Results GetJobResults(Guid jobID)
        {

            if (!IsJobFinished(jobID))
            {
                throw new InvalidOperationException();
            }

            return resultList[jobID];
        }

        // Job creation version that is used to create parallel compute tasks and return.
        // The caller can optionally specify a wait handle to be asynchronously notified upon job completion.
        // MidpointRounding is used in the Median calculation, if necessary.
        public static Guid StartCompute(uint[] inputs, MidpointRounding mode, EventWaitHandle waitHandle = null)
        {
            Guid taskName = new Guid();
            ResultSet r = new ResultSet() {ID = taskName};
            resultList.Add(taskName, null);
            if (waitHandle == null)
            {
                Task t = Task.Factory.StartNew(() =>
                {
                    r.Result = Compute(inputs, mode);
                    // Add key to result set
                    resultList[taskName] = r.Result;
                });
            }
            // else queueuserworkitem
            //ThreadPool.QueueUserWorkItem(waitHandle, r)

            return taskName;
        }

        // Synchronous call version which blocks while all computational tasks are completed for this job.
        // MidpointRounding is used in the Median calculation, if necessary.
        public static Results Compute(uint[] inputs, MidpointRounding mode)
        {
            return new Results(UIntMath.GCD(inputs), UIntMath.GetMedian(inputs, mode), UIntMath.GetMean(inputs));
        }

        // Inner class for holding results
        // Do NOT change this code.
        public class Results
        {
            public uint GCD { get;  }
            public uint Median { get;  }
            public double Mean { get;  }

            public Results(uint gcd, uint median, double mean)
            {
                GCD = gcd;
                Median = median;
                Mean = mean;
            }
        }

        public class ResultSet
        {
            public Guid ID { get; set; }
            public Results Result { get; set; }
        }
    }

    public static class UIntMath
    {
        public static uint GCD(uint[] inputs)
        {
            int count = inputs.Length;
            if (count == 0)
            {
                throw new InvalidOperationException("Empty collection");
            }
            return inputs.Aggregate(GCD);
        }
        static uint GCD(uint a, uint b)
        {
            return b == 0 ? a : GCD(b, a % b);
        }

        public static uint GetMedian(uint[] inputs, MidpointRounding mode)
        {
            Array.Sort(inputs);

            int count = inputs.Length;
            if (count == 0)
            {
                throw new InvalidOperationException("Empty collection");
            }
            else if (count % 2 == 0)
            {
                uint a = inputs[count / 2 - 1];
                uint b = inputs[count / 2];
                return (uint)Math.Round(((a + b) / 2m), mode);
            }
            else
            {
                return inputs[count / 2];
            }
        }

        public static double GetMean(uint[] inputs)
        {
            if (inputs.Length == 0)
            {
                throw new InvalidOperationException("Empty collection");
            }
            long sum = 0;
            long count = 0;
            checked
            {
                foreach (uint v in inputs)
                {
                    sum += v;
                    count++;
                }
            }
            if (count > 0) return (uint)((double)sum / count);
            throw new Exception();
        }
    }

    [Serializable]
    public class ObjectNotFoundException : Exception
    {
        public string ResourceReferenceProperty { get; set; }

        public ObjectNotFoundException()
        {
        }

        public ObjectNotFoundException(string message)
            : base(message)
        {
        }

        public ObjectNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected ObjectNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            ResourceReferenceProperty = info.GetString("ResourceReferenceProperty");
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            info.AddValue("ResourceReferenceProperty", ResourceReferenceProperty);
            base.GetObjectData(info, context);
        }

    }
}
